<?php

namespace SJRoyd\MF\VATStatus;

use SJRoyd\HTTPService\SoapRequest;

/**
 * Class VAT
 *
 * @package SJRoyd\MF\VATStatus
 * @see https://www.podatki.gov.pl/media/3275/specyfikacja-we-wy.pdf
 */
class VAT extends SoapRequest
{
    protected $ws_path   = 'https://sprawdz-status-vat.mf.gov.pl/';
    protected $soapVersion = SOAP_1_1;

    /**
     * Check VAT status by NIP
     *
     * @param   string  $nip
     * @return Response\SprawdzNip
     * @throws \Exception
     */
    public final function checkByNip($nip){
        /* @var $response Response\SprawdzNip */
        $response = $this->call(
            'SprawdzNip',
            new Request\SprawdzNip($nip),
            [200 => Response\SprawdzNip::class]
        );
        if($this->responseStatusCode != 200){
            throw new \Exception('An error occurred', $this->responseStatusCode);
        }
        return $response;
    }
}