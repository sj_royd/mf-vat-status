<?php

namespace SJRoyd\MF\VATStatus\Response;

class SprawdzNip {

    const CODE_N = 'N';
    const CODE_C = 'C';
    const CODE_Z = 'Z';
    const CODE_I = 'I';
    const CODE_D = 'D';
    const CODE_X = 'X';

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * @param   string  $code
     *
     * @return SprawdzNip
     */
    public function setKod($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @param   string  $message
     *
     * @return SprawdzNip
     */
    public function setKomunikat($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return in_array($this->code, [static::CODE_I, static::CODE_D, static::CODE_X]);
    }

}

/*
 * N - Podmiot o podanym identyfikatorze podatkowym NIP nie jest zarejestrowany jako podatnik VAT
 * C - Podmiot o podanym identyfikatorze podatkowym NIP jest zarejestrowany jako podatnik VAT czynny
 * Z - Podmiot o podanym identyfikatorze podatkowym NIP jest zarejestrowany jako podatnik VAT zwolniony
 * I - Błąd zapytania - Nieprawidłowy Numer Identyfikacji Podatkowej
 * D - Błąd zapytania - Data spoza ustalonego zakresu
 * X - Usługa nieaktywna
 */