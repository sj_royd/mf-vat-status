<?php

namespace SJRoyd\MF\VATStatus\Request;

class SprawdzNip
{
    public function __construct($nip)
    {
        $this->NIP = $nip;
    }

    public $NIP;
}