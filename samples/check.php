<?php

use SJRoyd\MF\VATStatus\VAT;

include '../vendor/autoload.php';

$status = (new VAT)->checkByNip('4658741012');

echo 'Code: '.$status->getCode().PHP_EOL;
echo 'Message: '.$status->getMessage().PHP_EOL;
echo 'Error: '.($status->isError() ? 'true' : 'false');