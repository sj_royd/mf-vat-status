# "Check VAT Status" service

The library allows you to check the taxpayer's VAT status using the official API of the Ministry of Finance in Poland

# Usage

```php
<?php

use SJRoyd\MF\VATStatus\VAT;

include '../vendor/autoload.php';

$status = (new VAT)->checkByNip('4658741012');

echo 'Code: '.$status->getCode().PHP_EOL;
echo 'Message: '.$status->getMessage().PHP_EOL;
echo 'Error: '.($status->isError() ? 'true' : 'false');
```

The meaning of codes:
- N - The entity with the provided tax ID NIP is not registered as a VAT taxpayer
- C - The entity with the tax ID provided is registered as an active VAT taxpayer
- Z - The entity with the provided tax ID number is registered as an exempt VAT taxpayer
- I - Inquiry error - Invalid Tax Identification Number
- D - Query error - Date outside of the specified range
- X - Service inactive

